package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        ///// Oppgave 6
        pokemon1 = new Pokemon("Mew", 20, 5);
        pokemon2 = new Pokemon("Mewtwo", 30, 7);

        System.out.println(pokemon1);
        System.out.println(pokemon2 + "\n");

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            if (pokemon1.isAlive()){
                pokemon1.attack(pokemon2);
            }
            if (pokemon2.isAlive()) {
                pokemon2.attack(pokemon1);
            }
        }

    }
}
